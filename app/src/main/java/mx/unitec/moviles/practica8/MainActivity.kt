package mx.unitec.moviles.practica8

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.weather_fragment.*
import mx.unitec.moviles.practica8.model.Coordenada
import mx.unitec.moviles.practica8.weather.WeatherFragment
import java.util.*

const val REQUEST_LOCATION_PERMISSION = 2

class MainActivity : AppCompatActivity() {

    private lateinit var mFuseLocationClient : FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private var requestingLocationUpdates =false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, WeatherFragment.newInstance())
                .commitNow()
        }


        mFuseLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationCallback =object : LocationCallback(){
            override fun onLocationResult(p0: LocationResult?) {
                p0 ?: return
                for (location in p0.locations){
                    val geocoder = Geocoder(applicationContext, Locale.getDefault())
                    val addresses : List<Address> = geocoder.getFromLocation(
                        location.latitude,
                        location.latitude,
                        1
                    )
                var address = ""
                for( i in 0..addresses[0].maxAddressLineIndex ){
                    address += addresses[0].getAddressLine(i)
                }
                    textview_location.text = getString(R.string.location_text,
                    location.latitude,
                    location.longitude,
                    location.time) + address
                }
            }
        }

    }

    fun clickButtonLocation(view: View) {
        getLocation()
    }

    private fun getLocation(){
        if(ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)

            {
                ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION_PERMISSION)
            } else {
                if(!requestingLocationUpdates){
                    requestingLocationUpdates =true
                    mFuseLocationClient.requestLocationUpdates(
                       getLocationRequest(),
                        locationCallback,
                        Looper.getMainLooper()
                    )
                } else {
                    requestingLocationUpdates = false
                    mFuseLocationClient.removeLocationUpdates(locationCallback)
                }

            }

        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION_PERMISSION)
        {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                getLocation()
            else
                Toast.makeText(this, R.string.location_permission_denied, Toast.LENGTH_LONG)
        }
    }

    private fun getLocationRequest(): LocationRequest? {
        val locationRequest = LocationRequest()
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 5000
        locationRequest.priority =LocationRequest.PRIORITY_HIGH_ACCURACY
        return locationRequest

    }

}